package com.kiran.softuserroomdatabase.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.kiran.softuserroomdatabase.R
import com.kiran.softuserroomdatabase.db.StudentDB
import com.kiran.softuserroomdatabase.entity.User
import kotlinx.coroutines.*

class SplashScreenActivity : AppCompatActivity() {
    var username: String? = ""
    var password: String? = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splashscreen)


        CoroutineScope(Dispatchers.Main).launch {
            delay(1000)
//            startActivity(Intent(this@SplashScreenActivity, LoginActivity::class.java))
            getUsernamePassword()
            login()
            finish()
        }
    }
    private fun getUsernamePassword(){
        val sharedPreferences = getSharedPreferences("usernamePasswordPref", MODE_PRIVATE)
        username = sharedPreferences.getString("username","" )
        password = sharedPreferences.getString("password","" )

    }
    private fun login() {
        val uname = username!!
        val pass = password!!
        var user: User? = null
        CoroutineScope(Dispatchers.IO).launch {
            user = StudentDB
                .getInstance(this@SplashScreenActivity)
                .getUserDAO()
                .checkUser(uname, pass)
            if (user == null) {
                startActivity(Intent(this@SplashScreenActivity, LoginActivity::class.java))
            } else {
                startActivity(Intent(this@SplashScreenActivity, DashboardActivity::class.java))
            }
        }

    }

}